//
//  ViewController.h
//  Calculator
//
//  Created by Jeremi Kaczmarczyk on 13.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *displayLabel;
@property (nonatomic) NSMutableArray *stack;

- (IBAction)buttonClicked:(id)sender;
- (NSString *)result: (NSMutableArray *)array;

@end

