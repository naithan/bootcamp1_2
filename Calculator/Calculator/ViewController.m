//
//  ViewController.m
//  Calculator
//
//  Created by Jeremi Kaczmarczyk on 13.04.2015.
//  Copyright (c) 2015 Jeremi Kaczmarczyk. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _stack = [[NSMutableArray alloc] init];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonClicked:(id)sender{
    NSString *button = [sender currentTitle];
    NSLog(@"%@",button);
    [_stack addObject:button];
    if ([button  isEqual: @"Enter"]) {
        [self result:_stack];
    }
}

- (NSString *)result: (NSMutableArray *)array{
    NSString *resultString;
    if ([self.stack[1] isEqual: @"+"]) {
        NSLog(@"Dziala dodawanie");
        int value1 = [self.stack[0] integerValue];
        int value2 = [self.stack[2] integerValue];
        int value3 = value1 + value2;
        resultString = [NSString stringWithFormat:@"%i",value3];
        _displayLabel.text = resultString;
    }
    else if ([self.stack[1] isEqual: @"-"]){
        NSLog(@"Odejmowanie");
        int value1 = [self.stack[0] integerValue];
        int value2 = [self.stack[2] integerValue];
        int value3 = value1 - value2;
        resultString = [NSString stringWithFormat:@"%i",value3];
        _displayLabel.text = resultString;
    }
    else if ([self.stack[1] isEqual: @"*"]){
        NSLog(@"Mnozenie");
        int value1 = [self.stack[0] integerValue];
        int value2 = [self.stack[2] integerValue];
        int value3 = value1 * value2;
        resultString = [NSString stringWithFormat:@"%i",value3];
        _displayLabel.text = resultString;
    }
    else if ([self.stack[1] isEqual: @"/"]){
        
    }
    return resultString;
}







@end
